# Copyright 2015-2017 Johannes Nixdorf <mixi@exherbo.org>
# Copyright 2013 Michael Forney
# Distributed under the terms of the GNU General Public License v2

SUMMARY="s6-networking is a suite of small networking utilities for Unix systems"
HOMEPAGE="http://skarnet.org/software/${PN}/"
DOWNLOADS="http://skarnet.org/software/${PN}/${PNV}.tar.gz"

LICENCES="ISC"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    nsss [[ description = [ Use the nsss library for user database lookups ] ]]
    static

    ( providers: bearssl libressl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-lang/execline[>=2.5.0.1]
        dev-libs/skalibs[>=2.7.0.0]
        net-dns/s6-dns[>=2.3.0.1]
        sys-apps/s6[>=2.7.2.0]

        nsss? ( sys-utils/nsss )
        providers:bearssl? ( dev-libs/bearssl[>=0.5] )
        providers:libressl? ( dev-libs/libressl:=[>=2.7.4] )
"

BUGS_TO="mforney@mforney.org"

# CROSS_COMPILE: because configure only expects executables to be prefixed if build != host
# REALCC: because the makefile prefixes configure's CC with $(CROSS_COMPILE)
DEFAULT_SRC_COMPILE_PARAMS=(
    REALCC=${CC}
    CROSS_COMPILE=$(exhost --target)-
)

src_configure()
{
    local args=(
        --enable-shared
        --with-lib=/usr/$(exhost --target)/lib
        --with-dynlib=/usr/$(exhost --target)/lib
        $(option_enable nsss)
        $(option_enable static allstatic)
        $(optionq providers:bearssl && echo "--enable-ssl=bearssl")
        $(optionq providers:libressl && echo "--enable-ssl=libressl")
    )

    [[ $(exhost --target) == *-gnu* ]] || \
        args+=( $(option_enable static static-libc) )

    econf "${args[@]}"
}

src_install()
{
    default

    insinto /usr/share/doc/${PNVR}/html
    doins -r doc/*
}

